import helpers as h
from datetime import time
import graphs as g


def print_top_photos_of_day(top_list_of_day, photos_upload_dt):
    top_stevec = len(top_list_of_day[-10:])

    for fav_no, fav_id in sorted(top_list_of_day)[-10:]:
        if fav_no > 0:
            print(
                f"{top_stevec}. Fotografija na povezavi http://flickr.com/photo.gne?id={fav_id} | "
                f"Priljubljenost: {fav_no} | "
                f"Čas objave: {photos_upload_dt.get(fav_id, 'ni podatka')}")

        top_stevec -= 1

    print()


# razvrščanje po intervalih glede na čas objave
def photos_day_intervals(photos_day, photos_upload_dt):
    day_intervals = {"0-2": [], "2-4": [], "4-6": [], "6-8": [], "8-10": [], "10-12": [],
                     "12-14": [], "14-16": [], "16-18": [], "18-20": [], "20-22": [], "22-24": []}

    for photo_favs, photo in photos_day:
        upload_time = photos_upload_dt[photo].time()

        if upload_time >= time(0, 0) and upload_time < time(2, 0):
            day_intervals["0-2"].append(photo_favs)
        elif upload_time >= time(2, 0) and upload_time < time(4, 0):
            day_intervals["2-4"].append(photo_favs)
        elif upload_time >= time(4, 0) and upload_time < time(6, 0):
            day_intervals["4-6"].append(photo_favs)
        elif upload_time >= time(6, 0) and upload_time < time(8, 0):
            day_intervals["6-8"].append(photo_favs)
        elif upload_time >= time(8, 0) and upload_time < time(10, 0):
            day_intervals["8-10"].append(photo_favs)
        elif upload_time >= time(10, 0) and upload_time < time(12, 0):
            day_intervals["10-12"].append(photo_favs)
        elif upload_time >= time(12, 0) and upload_time < time(14, 0):
            day_intervals["12-14"].append(photo_favs)
        elif upload_time >= time(14, 0) and upload_time < time(16, 0):
            day_intervals["14-16"].append(photo_favs)
        elif upload_time >= time(16, 0) and upload_time < time(18, 0):
            day_intervals["16-18"].append(photo_favs)
        elif upload_time >= time(18, 0) and upload_time < time(20, 0):
            day_intervals["18-20"].append(photo_favs)
        elif upload_time >= time(20, 0) and upload_time < time(22, 0):
            day_intervals["20-22"].append(photo_favs)
        elif upload_time >= time(22, 00) and upload_time <= time(23, 59, 59):
            day_intervals["22-24"].append(photo_favs)

    return day_intervals


# izračun povprečnega št. priljubljenosti fotografij v posameznem časovnem intervalu v dnevu
def avg_favs_day_intervals(day_intervals):
    avg_favs_interval = {}

    for interval, num_favs in day_intervals.items():
        # če je bila v določenem intervalu v dnevu sploh objavljena kakšna fotografija
        if len(num_favs) > 0:
            avg_favs_interval[interval] = sum(num_favs) / len(num_favs)

    return avg_favs_interval


# izris točkovnega grafa
def visualize_rv2(week_photos):
    g.scatter_by_week_days("Povprečno št. priljubljenosti fotografij glede na dan v tednu in čas objave v dnevu",
                           week_photos, "Časovni termini v dnevu", "Povprečno št. priljubljenosti", "Day")


def rv2_fav_dt_posted(text):
    photos = h.get_photos(text)
    photos_favorites = h.photos_get_favorites(photos)
    photos_upload_dt = h.photos_get_upload_date_time(photos)
    week_photos = {"monday": [], "tuesday": [], "wednesday": [], "thursday": [], "friday": [], "saturday": [],
                   "sunday": []}

    for photo in photos_upload_dt:
        match (photos_upload_dt[photo].weekday()):
            case 0:
                week_photos["monday"].append((photos_favorites.get(photo, 0), photo))
            case 1:
                week_photos["tuesday"].append((photos_favorites.get(photo, 0), photo))
            case 2:
                week_photos["wednesday"].append((photos_favorites.get(photo, 0), photo))
            case 3:
                week_photos["thursday"].append((photos_favorites.get(photo, 0), photo))
            case 4:
                week_photos["friday"].append((photos_favorites.get(photo, 0), photo))
            case 5:
                week_photos["saturday"].append((photos_favorites.get(photo, 0), photo))
            case 6:
                week_photos["sunday"].append((photos_favorites.get(photo, 0), photo))

    print("\n2. RAZISKOVALNO VPRAŠANJE")
    print("\n10 najbolj priljubljenih fotografij po dnevih v tednu")

    print("PONEDELJEK")
    print_top_photos_of_day(week_photos["monday"], photos_upload_dt)

    print("TOREK")
    print_top_photos_of_day(week_photos["tuesday"], photos_upload_dt)

    print("SREDA")
    print_top_photos_of_day(week_photos["wednesday"], photos_upload_dt)

    print("ČETRTEK")
    print_top_photos_of_day(week_photos["thursday"], photos_upload_dt)

    print("PETEK")
    print_top_photos_of_day(week_photos["friday"], photos_upload_dt)

    print("SOBOTA")
    print_top_photos_of_day(week_photos["saturday"], photos_upload_dt)

    print("NEDELJA")
    print_top_photos_of_day(week_photos["sunday"], photos_upload_dt)

    # povprečno št. priljubljenih v posameznem časovnem intervalu za vsak dan v tednu
    for day, photos in week_photos.items():
        week_photos[day] = avg_favs_day_intervals(photos_day_intervals(photos, photos_upload_dt))

    visualize_rv2(week_photos)
