import helpers as h
import graphs as g


def print_stats_5(photos_favorites, photos_cameras):
    # Izpis statistike in povprečja priljubljenosti posamezne fotografije
    try:
        avg_fav_photos = sum(photos_favorites.values()) / len(photos_favorites)
    except:
        avg_fav_photos = 0
    print("Št. slik:", len(photos_favorites))
    print(f"Povprečno št. priljubljenih na sliko: {avg_fav_photos:.2f}")

    try:
        percent_zero_fav_photos = (list(photos_favorites.values()).count(0) / len(photos_favorites)) * 100
    except:
        percent_zero_fav_photos = 0
    print("Št. slik brez priljubljenosti:", list(photos_favorites.values()).count(0))
    print(f"Delež slik brez priljubljenosti: {percent_zero_fav_photos:.2f} %")

    # iz slovarja v seznam terk, zaradi urejanja po št. priljubljenih v nadaljevanju
    top_list = h.get_list_value_key(photos_favorites)
    top_stevec = len(top_list[-5:])

    for fav_no, fav_id in sorted(top_list)[-5:]:
        print(
            f"{top_stevec}. Fotografija na povezavi http://flickr.com/photo.gne?id={fav_id} | "
            f"Priljubljenost: {fav_no} | "
            f"Posneta z napravo: {photos_cameras.get(fav_id, 'ni podatka')}")

        top_stevec -= 1


def favs_intervals(photos_favs):
    intervals_list = [0, 0, 0, 0, 0, 0, 0, 0]

    for favs in photos_favs.values():
        if favs > 0 and favs <= 10:
            intervals_list[0] += 1
        elif favs > 10 and favs <= 30:
            intervals_list[1] += 1
        elif favs > 30 and favs <= 50:
            intervals_list[2] += 1
        elif favs > 50 and favs <= 100:
            intervals_list[3] += 1
        elif favs > 100 and favs <= 200:
            intervals_list[4] += 1
        elif favs > 200 and favs <= 500:
            intervals_list[5] += 1
        elif favs > 500 and favs <= 1000:
            intervals_list[6] += 1
        elif favs > 1000:
            intervals_list[7] += 1

    return intervals_list


def remove_zero_favs_interval(list_favs, labels):
    favs_no_zero = []
    labels_no_zero = []

    for favs, label in zip(list_favs, labels):
        if favs > 0:
            favs_no_zero.append(favs)
            labels_no_zero.append(label)

    return favs_no_zero, labels_no_zero


def visualize_rv1(title, photos_favs):
    labels = ["1-10", "11-30", "31-50", "51-100", "101-200", "201-500", "501-1000", ">1000"]
    percent_fav_cameras, labels = remove_zero_favs_interval(favs_intervals(photos_favs), labels)

    g.pie_chart(title, percent_fav_cameras, labels, "Intervali priljubljenosti")


def rv1_fav_camera_phone(text):
    photos = h.get_photos(text)
    photos_favorites = h.photos_get_favorites(photos)
    photos_cameras = h.photos_get_camera(photos)
    by_cameras = {}
    by_phones = {}

    for photo in photos_favorites:
        if photo in photos_cameras:
            if photos_cameras[photo].split()[0].lower() in h.cameras:
                by_cameras[photo] = photos_favorites[photo]
            else:
                by_phones[photo] = photos_favorites[photo]

    print("\n1. RAZISKOVALNO VPRAŠANJE")

    # 5 najbolj priljubljenih fotografij
    print("\n5 najbolj priljubljenih fotografij ustvarjenih s fotoaparati")
    print_stats_5(by_cameras, photos_cameras)

    print("\n5 najbolj priljubljenih fotografij ustvarjenih s telefoni")
    print_stats_5(by_phones, photos_cameras)

    visualize_rv1("Priljubljenost slik ustvarjenih s fotoaparati", by_cameras)
    visualize_rv1("Priljubljenost slik ustvarjenih s telefoni", by_phones)
