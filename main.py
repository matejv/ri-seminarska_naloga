import rv1
import rv2
import rv3

if __name__ == '__main__':
    print(
        "## Ali so bolj priljubljene fotografije narave Ljubljane, ki so ustvarjene s fotoaparati ali mobilnimi telefoni? ##")
    rv1.rv1_fav_camera_phone("nature ljubljana")
    print(
        "## Ali so bolj priljubljene fotografije narave Maribora, ki so ustvarjene s fotoaparati ali mobilnimi telefoni? ##")
    rv1.rv1_fav_camera_phone("nature maribor")

    print(
        "## Ali objava fotografij sebkov (angl. selfie) ob priporočenih terminih vpliva na večjo priljubljenost v Sloveniji? ##")
    rv2.rv2_fav_dt_posted("selfie slovenia")
    print("## Ali objava fotografij hrane v času kosila vpliva na večjo priljubljenost v Ljubljani? ##")
    rv2.rv2_fav_dt_posted("food ljubljana")

    print("## Ali so v Sloveniji bolj priljubljene fotografije ženskih portretov ali moških portretov? ##")
    rv3.rv3_fav_tags1_tags2("portrait,woman,slovenia", "portrait,man,slovenia")
    print("## Ali so bolj priljubljene fotografije narave Slovenije ali fotografije živali Slovenije? ##")
    rv3.rv3_fav_tags1_tags2("nature,slovenia", "animals,slovenia")
