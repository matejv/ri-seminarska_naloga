import flickrapi
import json
from datetime import datetime

# TODO: Enter your Flickr KEY and SECRET
FLICKR_PUBLIC = 'YOUR_FLICKR_KEY'
FLICKR_SECRET = 'YOUR_FLICKR_SECRET'
flickr = flickrapi.FlickrAPI(FLICKR_PUBLIC, FLICKR_SECRET, format='json')

cameras = ("canon", "nikon", "sony", "fujifilm", "panasonic", "olympus", "ricoh", "leica", "dji", "pentax", "gopro",
           "hasselblad", "kodak", "sigma", "konica minolta", "casio", "hp", "phase one", "kyocera", "leaf", "helio",
           "vivitar", "sanyo", "concord", "toshiba", "epson", "polaroid", "jvc")
phones = ("apple", "samsung", "google", "huawei", "xiaomi", "oneplus", "nokia", "blackberry", "motorola",
          "sony ericsson", "lg", "htc", "palm", "acer", "nintendo", "benq", "docomo", "sharp", "kddi")


def parse_reply(reply, attribute):
    return json.loads(json.dumps(json.loads(reply)[attribute]))


# Flickr will return at most the first 4000 results for any given search query
# Per page is default 100 photos
def get_num_pages(reply):
    pages = reply['pages']

    if pages > 40:
        print("OPOZORILO: Poizvedba vsebuje več kot 4000 rezultatov. Zajetih bo največ prvih 4000.")
        return 40

    return pages


# get photos by text search according to title, description, tags
def get_photos(search_text):
    reply = parse_reply(flickr.photos.search(text=search_text), "photos")  # default: per_page=100, page=1
    pages = get_num_pages(reply)
    photos = set()

    for i in range(pages):
        page_photos = parse_reply(flickr.photos.search(text=search_text, page=i), "photos")['photo']

        for p in page_photos:
            photos.add(p['id'])

    return photos


# get number of favorites
def photos_get_favorites(photos):
    favorites_no = {}

    for photo in photos:
        try:
            reply = parse_reply(flickr.photos.getFavorites(photo_id=photo, per_page=1), "photo")
            favorites_no[photo] = reply['total']
        except:
            print("Napaka pri pridobivanju št. priljubljenosti slike z ID:", photo)

    return favorites_no


# get camera info
def photos_get_camera(photos):
    photo_camera = {}

    for photo in photos:
        try:
            reply = parse_reply(flickr.photos.getExif(photo_id=photo), "photo")

            if len(reply['camera']) > 0:
                photo_camera[photo] = reply['camera']
        except:
            print("Napaka pri pridobivanju podatkov EXIF slike z ID:", photo)

    return photo_camera


# get upload date and time
def photos_get_upload_date_time(photos):
    photo_upload_dt = {}

    for photo in photos:
        try:
            reply = parse_reply(json.dumps(parse_reply(flickr.photos.getInfo(photo_id=photo), "photo")), "dates")

            # datum objave je v obliki UNIX timestamp
            if len(reply['posted']) > 0:
                photo_upload_dt[photo] = datetime.fromtimestamp(int(reply['posted']))
        except:
            print("Napaka pri pridobivanju datuma nalaganja slike z ID:", photo)

    return photo_upload_dt


# get photos by tags
def get_photos_tags(tags):
    # tag_mode = all (AND operator), tag_mode = any (OR operator; default)
    reply = parse_reply(flickr.photos.search(tags=tags, tag_mode="all"), "photos")
    pages = get_num_pages(reply)
    photos = set()

    for i in range(pages):
        page_photos = parse_reply(flickr.photos.search(tags=tags, tag_mode="all", page=i), "photos")['photo']

        for p in page_photos:
            photos.add(p['id'])

    return photos


# from dictionary creates list of (value, key) pairs for sorting by values
def get_list_value_key(dict):
    vk_list = []

    for key, value in dict.items():
        vk_list.append((value, key))

    return vk_list
