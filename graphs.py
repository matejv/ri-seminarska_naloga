import matplotlib.pyplot as plt
import numpy as np


def histogram(title, xpoints, xlabel, ylabel):
    xpoints = np.array(xpoints)
    plt.title(title)
    plt.hist(xpoints)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()


def pie_chart(title, pieces, labels, legend_title):
    pieces = np.array(pieces)
    plt.title(title)
    plt.pie(pieces, labels=labels, autopct="%1.2f%%")
    plt.legend(title=legend_title)
    plt.show()


def scatter_by_week_days(title, week_favs_intervals, xlabel, ylabel, legend_title):
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    for day, favs_intervals in week_favs_intervals.items():
        x = np.array(list(favs_intervals.keys()))
        y = np.array(list(favs_intervals.values()))
        plt.scatter(x, y, label=day)

    plt.legend(title=legend_title)
    plt.show()
