import helpers as h
import graphs as g


def print_top_photos(photos_favorites):
    top_list = h.get_list_value_key(photos_favorites)
    top_stevec = len(top_list[-10:])

    for fav_no, fav_id in sorted(top_list)[-10:]:
        if fav_no > 0:
            print(
                f"{top_stevec}. Fotografija na povezavi http://flickr.com/photo.gne?id={fav_id} | "
                f"Priljubljenost: {fav_no})")

        top_stevec -= 1

    print()


# remove photos with zero favorites
def remove_no_favs(photos_favorites):
    for key, value in photos_favorites.copy().items():
        if value == 0:
            del photos_favorites[key]

    return photos_favorites


def visualize_rv3(tags, photos_favorites):
    g.histogram("Priljubljenost fotografij s ključniki: " + tags,
                list(remove_no_favs(photos_favorites).values()), "Priljubljenost", "Frekvenca")


def rv3_fav_tags1_tags2(tags1, tags2):
    tags1_photos_favorites = h.photos_get_favorites(h.get_photos_tags(tags1))
    tags2_photos_favorites = h.photos_get_favorites(h.get_photos_tags(tags2))

    print("\n3. RAZISKOVALNO VPRAŠANJE")

    # Izpis statistike za slike s 1. nizom ključnikov
    print("1. niz ključnikov:", tags1)
    print("Št. slik s 1. nizom ključnikov:", len(tags1_photos_favorites))
    try:
        avg_fav_tags1 = sum(tags1_photos_favorites.values()) / len(tags1_photos_favorites)
    except:
        avg_fav_tags1 = 0
    print(f"Povprečno št. priljubljenih na sliko s 1. nizom ključnikov: {avg_fav_tags1:.2f}")

    print("\n10 najbolj priljubljenih fotografij s 1. nizom ključnikov")
    print_top_photos(tags1_photos_favorites)
    visualize_rv3(tags1, tags1_photos_favorites)

    # Izpis statistike za slike z 2. nizom ključnikov
    print("2. niz ključnikov:", tags2)
    print("Št. slik z 2. nizom ključnikov:", len(tags2_photos_favorites))
    try:
        avg_fav_tags2 = sum(tags2_photos_favorites.values()) / len(tags2_photos_favorites)
    except:
        avg_fav_tags2 = 0
    print(f"Povprečno št. priljubljenih na sliko z 2. nizom ključnikov: {avg_fav_tags2:.2f}")

    print("\n10 najbolj priljubljenih fotografij z 2. nizom ključnikov")
    print_top_photos(tags2_photos_favorites)
    visualize_rv3(tags2, tags2_photos_favorites)
